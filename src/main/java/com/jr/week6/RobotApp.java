package com.jr.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot rukawa = new Robot("Rukawa", 'R', 10, 10);
        Robot sendoh = new Robot("Sendoh", 'S', 8, 8);
        rukawa.print();
        rukawa.right();
        rukawa.print();
        rukawa.down();
        rukawa.print();
        sendoh.print();
        sendoh.left();
        sendoh.print();
        sendoh.up();
        sendoh.print();

        for(int y = Robot.Y_MIN ; y <= Robot.Y_MAX; y++){
            for(int x = Robot.X_MIN ; x<= Robot.X_MAX;x++){
                if(rukawa.getX()== x && rukawa.getY() == y){
                    System.out.print(rukawa.getSymbol());
                }else if(sendoh.getX()== x && sendoh.getY() == y){
                    System.out.print(sendoh.getSymbol());
                }else{
                    System.out.print("-");
                }
            }System.out.println();
        }
    }
}
