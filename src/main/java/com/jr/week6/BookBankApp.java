package com.jr.week6;

public class BookBankApp {
    public static void main(String[] args){
        BookBank jr = new BookBank("JR",100.0);
        jr.print();
        jr.deposit(901);
        jr.print();
        jr.withdraw(1);
        jr.print();

        BookBank rj = new BookBank("RJ",10000.0);
        rj.print();
        rj.deposit(120100);
        rj.print();
        rj.withdraw(129100);
        rj.print();

        BookBank jj = new BookBank("JJ",1.0);
        jj.print();
        jj.deposit(9825);
        jj.print();
        jj.withdraw(8826);
        jj.print();
    }
}
