package com.jr.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    
    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Test1",0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0 , book.getBalance() , 0.00001);
    }
    @Test
    public void shouldDepositNegative() {
        BookBank book = new BookBank("Test2",0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0 , book.getBalance() , 0.00001);
    }

    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Test3",0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0 , book.getBalance() , 0.00001);
    }

    @Test
    public void shouldWithdrawNegative() {
        BookBank book = new BookBank("Test4",0);
        book.deposit(50);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(50 , book.getBalance() , 0.00001);
    }

    @Test
    public void shouldWithdrawOverBalance() {
        BookBank book = new BookBank("Test5",0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50.0 , book.getBalance() , 0.00001);
    }

    @Test
    public void shouldWithdraw100balance100() {
        BookBank book = new BookBank("Test6",0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0 , book.getBalance() , 0.00001);
    }
}
